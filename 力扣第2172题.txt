class Solution {
public:
    int dfs(int index,vector<int>& nums, int numSlots, vector<int>& basket,int state)
    {
        if(index==nums.size())  
          return 0;
        if(dp[state]!=-1)
           return dp[state];
        int ans=0;
        int x1=0;
        for(int i=0;i<numSlots;i++)
        {
            int sum=nums[index]&(i+1);
            int p1=(state>>(i*2))&1;
            int p2=(state>>(i*2+1))&1;
            if(p1==0&&p2==0||p1==0&&p2==1)
            {
                x1=sum+dfs(index+1,nums,numSlots,basket,state|(1<<(i*2)));
            }
            else if(p1==1&&p2==0)
            {
                x1=sum+dfs(index+1,nums,numSlots,basket,state|(1<<(i*2+1))^(1<<(i*2)));
            }
            ans=max(x1,ans);
        }
        dp[state]=ans;
        return ans;
    }
    int maximumANDSum(vector<int>& nums, int numSlots) {
        vector<int> basket(numSlots,0);
        dp.resize(1<<(numSlots*2),-1);
        return dfs(0,nums,numSlots,basket,0);
    }
    vector<int> dp;
};